/*
 * This work is licensed under a Creative Commons Attribution-ShareAlike 3.0 Unported License (http://creativecommons.org/licenses/by-sa/3.0/)
 */
package com.jeremyvinding.java.common;

import java.io.Closeable;
import java.util.ArrayList;
import java.util.concurrent.Callable;

public abstract class AutoClosingCallable<T> implements Callable<T>{

    protected abstract T doWork() throws Exception;

    @Override
    @SuppressWarnings("ThrowFromFinallyBlock")
    public final T call() throws Exception {
        Throwable pending = null;
        T returnValue = null;
        try {
            returnValue = doWork();
        } catch (final Throwable t) {
            pending = t;
        } finally {
            for (final Closeable closeable : mCloseables) {
                try {
                    closeable.close();
                } catch (final Throwable t) {
                    if (null == pending) {
                        pending = t;
                    }
                }
                if (null != pending) {
                    if (pending instanceof RuntimeException) {
                        throw (RuntimeException)pending;
                    } else if (pending instanceof Error) {
                        throw (Error)pending;
                    }
                    throw (Exception)pending;
                }
            }
        }
        return returnValue;
    }

    protected final <C extends Closeable> C autoclose(final C closeable) {
        mCloseables.add(0, closeable);
        return closeable;
    }

    private final ArrayList<Closeable> mCloseables = new ArrayList<Closeable>();
}
